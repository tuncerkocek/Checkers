package com.g40631.tuncer.jeudedame.model;

import java.util.ArrayList;

/**
 * Created by tuncer on 05-11-17.
 */

public class Piece {
    private Color colorOfPiece;

    public Piece(Color color) {
        colorOfPiece = color;
    }

    public Color getColorOfPiece() {
        return colorOfPiece;
    }
}
