package com.g40631.tuncer.jeudedame.model;

/**
 * Created by tuncer on 05-11-17.
 */

public class Case {
    private Position position;
    private Piece actualPiece;
    private Color colorCase;
    private boolean thereIsPiece=true;
    public Case(int x, int y) {
        position = new Position(x, y);
        if (position.getX() + position.getY() % 2 == 0) {
            colorCase = Color.BLANC;
        } else {
            colorCase = Color.NOIR;
        }
        if(position.getX()<3){
            if(colorCase==Color.BLANC){
                actualPiece=new Piece(Color.NOIR);
            }
        }else if(position.getX()>4){
            if(colorCase==Color.NOIR){
                actualPiece=new Piece(Color.BLANC);
            }
        }else{
            thereIsPiece=false;
        }
    }
    public Color getPieceColor(){
        return actualPiece.getColorOfPiece();
    }
    public boolean isThereIsPiece() {
        return thereIsPiece;
    }

    public Position getPosition() {
        return position;
    }

    public Piece getActualPiece() {
        return actualPiece;
    }

    public Color getColorCase() {
        return colorCase;
    }

    public void setActualPiece(Piece actualPiece) {
        if(actualPiece==null){
            thereIsPiece=false;
        }else{
            thereIsPiece=true;
        }
        this.actualPiece = actualPiece;
    }
    public boolean isDifferentColor(Piece piece){
        return actualPiece.getColorOfPiece()!=piece.getColorOfPiece();
    }
}
