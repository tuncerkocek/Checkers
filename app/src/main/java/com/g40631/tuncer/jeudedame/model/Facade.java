package com.g40631.tuncer.jeudedame.model;

/**
 * Created by tuncer on 05-11-17.
 */

public class Facade {
    private Board board;

    public Facade() {

    }
    public void nouvellePartie(){
        this.board = new Board();
    }
    public void chargerPartie(){

    }

    public void multiplayer(){

    }
    public Case[][] getBoard(){
        return board.getLePlateau();
    }
}

