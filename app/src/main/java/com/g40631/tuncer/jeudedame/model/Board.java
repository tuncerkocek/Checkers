package com.g40631.tuncer.jeudedame.model;


import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by tuncer on 05-11-17.
 */

public class Board {
    private Case[][] lePlateau;
    private ArrayList<Player> players;
    private int currentPlayer;

    public Board() {
        this.lePlateau = new Case[8][8];
        for (int x = 0; x < lePlateau.length; x++) {
            for (int y = 0; y < lePlateau[0].length; y++) {
                lePlateau[x][y] = new Case(x, y);
            }
        }
    }

    public Case[][] getLePlateau() {
        return Arrays.copyOf(lePlateau, lePlateau.length);
    }

    private void movePiece(Position oldPosition,Position  newPosition){
        lePlateau[newPosition.getX()][newPosition.getY()].setActualPiece(lePlateau[oldPosition.getX()][oldPosition.getY()].getActualPiece());
        lePlateau[oldPosition.getX()][oldPosition.getY()].setActualPiece(null);
    }
    public void canWeMove(Position oldPosition,Position  newPosition){
       movePiece(oldPosition,newPosition);
    }
}
