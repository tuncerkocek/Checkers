package com.g40631.tuncer.jeudedame.model;

/**
 * Created by tuncer on 05-11-17.
 */

public enum Color {
    BLANC("Blanc"),
    NOIR("Noir");
    private String color;
    Color(String color) {
        this.color=color;
    }

    public String getColor() {
        return color;
    }
}
